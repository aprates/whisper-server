#!/usr/bin/env python

# Standalone transcribe script

# This script transcribes audio files using the OpenAI Whisper model.
# It takes a path to an audio file as argument and prints transcription.
# Example usage: ./transcribe.py /path/to/audiofile

import sys
import os
from contextlib import contextmanager


# Check if file path argument was provided
if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    print("Error: No audio file path provided.")
    print("Usage: ./transcribe.py /path/to/audiofile")
    sys.exit()


# Context manager to suppress console output
# This is useful to hide warning messages from the
# transformers library that clutter the console and
# ensure we only print the actual transcription.
@contextmanager
def suppress_console_output():
    with open(os.devnull, "w") as null_file:
        old_stdout = os.dup(sys.stdout.fileno())
        old_stderr = os.dup(sys.stderr.fileno())

        sys.stdout.flush()
        sys.stderr.flush()
        os.dup2(null_file.fileno(), sys.stdout.fileno())
        os.dup2(null_file.fileno(), sys.stderr.fileno())

        try:
            yield
        finally:
            sys.stdout.flush()
            sys.stderr.flush()
            os.dup2(old_stdout, sys.stdout.fileno())
            os.dup2(old_stderr, sys.stderr.fileno())
            os.close(old_stdout)
            os.close(old_stderr)


# Initialize the pipeline
with suppress_console_output():
    import torch
    from transformers import pipeline

    pipe = pipeline("automatic-speech-recognition",
                    "openai/whisper-large-v2",
                    device="cpu")  # device="cuda:0" for Nvidia GPU

    # see: https://huggingface.co/docs/transformers/v4.34.1/en/main_classes/model#transformers.PreTrainedModel.to_bettertransformer
    pipe.model = pipe.model.to_bettertransformer()


# Process the audio file and obtain the transcription
output = pipe(filename,
              chunk_length_s=30,
              batch_size=8)


# Print the transcribed text
print(output['text'].strip())
