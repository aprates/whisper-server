#!/usr/bin/env bash

# Whisper-Server Project <https://gitlab.com/aprates/whisper-server>
# Initially developed by Antonio Prates
# Licensed under MIT

# Halt script on error
set -e

# Init DOCKER_USERNAME and DOCKER_PASSWORD
. docker_credentials

# Gather metadata
IMAGE_NAME="$DOCKER_USERNAME/whisper-server"

# Build a fresh image
docker build --tag "$IMAGE_NAME" .

# Publish the image
echo
echo 'Logging in to DockerHub...'
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
echo
echo 'Pushing image to DockerHub...'
docker push "$IMAGE_NAME"
docker logout
