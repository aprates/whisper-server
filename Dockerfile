# Whisper-Server Project <https://gitlab.com/aprates/whisper-server>
# Initially developed by Antonio Prates
# Licensed under MIT

# Using official Python runtime as the parent image
FROM python:3.8-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /app

# Install system dependencies
RUN apt-get update \
  && apt-get install -y --no-install-recommends netcat-openbsd gcc git ffmpeg \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install Python dependencies
COPY requirements.txt /app/requirements.txt
RUN pip install --upgrade pip \
  && pip install --no-cache-dir -r requirements.txt

# Cache the Whisper model
COPY preload_model.py /app/preload_model.py
RUN python /app/preload_model.py

# Copy all other project files
COPY . /app/

# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
# For environments with multiple CPU cores, increase the number of workers
# to be equal to the cores available.
CMD exec gunicorn --bind :8000 --workers 1 --threads 8 --timeout 0 server:app
