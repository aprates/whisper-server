#!/usr/bin/env python

# Whisper-Server Project <https://gitlab.com/aprates/whisper-server>
# Initially developed by Antonio Prates
# Licensed under MIT

from flask import Flask, request, jsonify, current_app
import os
import logging
import torch
from transformers import pipeline

app = Flask(__name__)

# Set up the base directory
base_dir = "/work"


# Initialize the pipeline
gunicorn_logger = logging.getLogger('gunicorn.error')
gunicorn_logger.info("Initializing whisper pipeline...")
pipe = pipeline("automatic-speech-recognition",
                "openai/whisper-large-v2",
                device="cpu")  # device="cuda:0" for Nvidia GPU
pipe.model = pipe.model.to_bettertransformer()
gunicorn_logger.info("Whisper pipeline is ready.")


# Server routes

@app.route('/health', methods=['GET'])
def health_check():
    return "Whisper-Server is up!\n"


@app.route('/files', methods=['GET'])
def list_files():
    try:
        files = os.listdir(base_dir)
        return jsonify(files)
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/transcribe', methods=['POST'])
def transcribe():
    # Check if a filename was provided
    if 'filename' not in request.json:
        return jsonify({'error': 'No filename provided'}), 400

    input_filename = request.json['filename']
    output_filename = f"{os.path.splitext(input_filename)[0]}.txt"

    input_file_path = os.path.join(base_dir, input_filename)
    output_file_path = os.path.join(base_dir, output_filename)

    if not os.path.exists(input_file_path):
        return jsonify({'error': 'File not found'}), 404

    gunicorn_logger.info(f'Processing {input_filename}')

    try:
        output = pipe(input_file_path,
                      chunk_length_s=30,
                      batch_size=8)

        # Save the output to a text file
        with open(output_file_path, 'w') as f:
            f.write(output['text'].strip())

        success_message = f'Transcription written to {output_filename}'
        gunicorn_logger.info(success_message)
        return jsonify({'message': success_message}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
