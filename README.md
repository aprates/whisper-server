# Whisper-Server

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

`Whisper-Server` is a project that leverages the capabilities of OpenAI's models for automatic speech recognition (ASR), providing a fast and efficient service for converting Speech-to-Text (STT). This project is inspired by and based on the [Insanely Fast Whisper](https://github.com/Vaibhavs10/insanely-fast-whisper) guide.

## Features

- Fast and accurate speech-to-text conversion, optimized through [Hugging Face Optimum](https://pypi.org/project/optimum/)
- Easy integration, with a containerized solution for hassle-free deployment

## Usage

### Running the Server

To initiate the server, execute the following command to spin up a container utilizing the provided Docker image and establish the required port mappings:

```bash
docker run --rm -it -p 8000:8000 -v ./samples:/work antonioprates/whisper-server
```

This command launches a fresh Docker container based on the `antonioprates/whisper-server` image, linking the local port 8000 to the identical port within the container. Simultaneously, it sets up a volume that associates the `samples` directory from your local environment to the `/work` directory in the container, enabling file access and storage by the application.

> be warned, the Docker image is large, with over 6 GB in compressed size

### Interacting with the Server

Once the server is up and running, you can interact with its API using `curl` or any HTTP client of your choice. Below are the available endpoints and how to interact with them:

#### health

```bash
curl http://0.0.0.0:8000/health
```

This command checks the health of the server. If the server is running, it will respond with "Whisper-Server is up!"

#### files

```bash
curl http://0.0.0.0:8000/files
```

This command returns a JSON array of strings listing the filenames available in the `/work` directory inside the container.

#### transcribe

```bash
curl -X POST http://0.0.0.0:8000/transcribe -H "Content-Type: application/json" -d '{"filename":"gettysburg.wav"}'
```

This command sends a request to transcribe the audio file specified by `"filename"`. The server should respond with a message indicating the success of the transcription and the name of the output file, such as: `{"message":"Transcription written to gettysburg.txt"}`

### Building Your Own Version

If you prefer, you can construct a customized version by building your own Docker image:

```bash
docker build --tag <your_custom_image_name> .
```

### Standalone script

A simple Python script is also provided with this repo for standalone use.

First, install the project dependencies:

```bash
pip install -r requirements.txt
```

Then run the script like so:

```bash
./transcribe.py samples/gettysburg.wav
```

Expected result on stdout:

```
Four score and seven years ago, our fathers brought forth on this continent a new nation, conceived in liberty and dedicated to the proposition that all men are created equal.
```

## Contributions

Contributions to `Whisper-Server` are welcome! If you have suggestions for improvements or new features, please create an issue or submit a merge request on the [GitLab repository](https://gitlab.com/aprates/whisper-server).

### Donations

If you find `Whisper-Server` useful and would like to say thanks, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## Acknowledgments

Special thanks to Vaibhav Srivastav, the author of "Insanely Fast Whisper" for inspiring this project and to the OpenAI team for their work on the Whisper model.

## License

[MIT](LICENSE) © 2023 Antonio Prates
