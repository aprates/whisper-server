#!/usr/bin/env python

# Whisper-Server Project <https://gitlab.com/aprates/whisper-server>
# Initially developed by Antonio Prates
# Licensed under MIT

import torch
from transformers import pipeline

# same model we use on other scripts
pipe = pipeline("automatic-speech-recognition",
                "openai/whisper-large-v2",
                device="cpu") # device="cuda:0" for Nvidia gpu
